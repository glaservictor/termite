#include <bits/stdc++.h>
#include "include/Grille.hpp"
#include "include/Termite.hpp"
#include <stdio.h>
#include "include/environement.hpp"
#include "include/testStructure.hpp"

#define ASSERT(test) if (!(test)) std::cout << "Test failed in file " << __FILE__ \
                                       << " line " << __LINE__ << ": " #test << std::endl

using namespace std;


void testTourne()
{
    termite t(1,1,1);
    t.tourneLibre(1);
    ASSERT(t.directionTermite() == 1);
    t.tourneLibre(3);
    ASSERT(t.directionTermite() == 3);
}

void testPrend()
{
    termite t(1,1,1);
    t.prendBrindille();
    ASSERT(t.porteBrindille() == true);
    t.poseBrindille();
    ASSERT(t.porteBrindille() ==false );
}

void testAvance()
{
    termite t(1,1,1);
    t.tourneLibre(0);
    t.avance();
    ASSERT(t.directionTermite()==nordOuest);
    ASSERT(t.accessCoord().first == 0);
    ASSERT(t.accessCoord().second == 0);
}

void testEstVide()
{
    termite t(1,1,1);
    grille g;
    g.poseTermite(t.accessCoord(),1);
    ASSERT(g.estVide({1,1}) == false);
    ASSERT(g.estVide({0,0}) == true);
    
}

void testDansGrille()
{
    termite t(0,0,0);
    t.tourneLibre(0);
    t.dansGrille({20,20});
    //ASSERT(t.changeOccur({20,20}));
    ASSERT(t.accessCoord().first== 19);
    ASSERT(t.accessCoord().second == 19);
}

void testBrindille()
{
    grille g;
    termite t(1,1,1);
    g.poseBrindille({2,1});
    ASSERT(g.contientBrindille({1,1}) == false);
    ASSERT(g.contientBrindille({2,1}) == true);
}

void testPoseBrindille()
{
    grille g;
    termite t(1,1,1);
    g.poseBrindille({1,2});
    ASSERT(g.contientBrindille({1,2})==true);
    ASSERT(g.accesGrille()[1][2]==-1);
    g.eneleveBrindille({1,2});
    ASSERT(g.estVide({1,2}));
    ASSERT(g.accesGrille()[1][2]==0);
}

void testEnleveBrindille()
{
    grille g;
    vector<int> b(10,0);
    termite t(1,1,1);
    g.poseBrindille({1,2});
    ASSERT(g.contientBrindille({1,2})==true);
    g.eneleveBrindille({1,2});
    ASSERT(g.contientBrindille({1,2})==false);
    ASSERT(b[1]==0);
}

void testPoseTermite()
{
    grille g;
    g.poseTermite({1,1},1);
    ASSERT(g.estVide({1,1})==false);
    g.eneleveTermite({1,1});
    ASSERT(g.estVide({1,1})==true);
}


void testEnleveBrin()
{
    grille g;
    g.poseBrindille({4,5});
    g.poseBrindille({7,5});
    ASSERT(g.contientBrindille({4,5})==true);
    ASSERT(g.contientBrindille({7,5})==true);
}

void testPositionLibre()
{
    grille g;
    termite t(1,1,2);
    g.poseTermite({0,0},1);
    g.poseTermite({1,0},1);
    g.poseTermite({0,1},1);
    g.poseTermite({2,2},1);
    g.poseTermite({1,2},1);
    g.poseTermite({2,1},1);
    g.poseTermite({2,0},1);
    positionLibre(g,t);
    ASSERT(t.directionTermite() == nordEst);
    

}


void SUPERSTRUCT()
{
testPositionLibre();
testEstVide();
testDansGrille();
testBrindille();
testPoseBrindille();
testEnleveBrindille();
testPoseTermite();
testTourne();
testPrend();
testAvance();
testEnleveBrin();

}


