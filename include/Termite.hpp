
#ifndef TERMITE_INC
#define TERMITE_INC
#include "Grille.hpp"
#include<bits/stdc++.h>

#define coord pair<int,int>
using namespace std;

enum dir : int {nordOuest = 0, est = 1, nord = 2, sudOuest = 3, sudEst = 4, sud = 5 ,nordEst = 6, ouest = 7};


/////////////////////////////////////////////////////////
//		       fonction du type termite 	           //
/////////////////////////////////////////////////////////


class termite{
	public:



		termite(int n, int m,int i);

		/** accessseur pour connaitre la direction d'une termite 
		 * @return la direction associée 
		 **/
		dir directionTermite();

		/** renvoit les coordonnés devant la termite
		 * @return une paire de coordonées
		 **/
		pair<int,int> devantTermite();

		/** test si la termite porte une brindille ou non (accesseur)
		 * @return true si elle porte une brindille
		 **/
		bool porteBrindille();
			
		/** modifie les coordonnées de la termite vers la droite
		 **/
		void tourneADroite();

		/** modifie les coordonnées de la termite vers la gauche 
		 **/
		void tourneAGauche();

		/** modifie les coordonnées de la termite vers une direction aleatoire
		 **/
		void tourneAleat();

		/**tourne selon une direction donné casté en chiffre
		 * @param[in] entier compris entre 0 et 7
		 **/
		void tourneLibre(int c);

		/**accesseur pour les coordonnées d'une termite 
		 * @return les coordonne de la termite
		 **/
		pair<int,int> accessCoord();

		/** modifie les coordonées de la termite d'une case en avant 
		 **/
		void avance();

		/**porte brindille 
		 * @param[in] modifie l'etat du termite sur le port d'une brindille
		 **/
		void porteBrindille(bool brin);
		/** accesseur pout déterminer depuis combien de temps la brindille est portée 
		 **/
		int tempsBrin();

		/** pose brindille permet de remettre a 0 les compteurs du termite 
		 **/
		void poseBrindille();

		/** change l'etat de la termite pour porter une brindille
		 **/
		void prendBrindille();
		
		/** Accesseur pour le sablier
		 * @return m_sablier 
		 **/
		int sablier();

		/** accesseur pour le numero de la termite
		 **/
		int numero();

		/** permet d'agir comme un donnut plutôt qu'un Kré 
		 * @param[in] la taille de la grille
		 * @return true si modification il y a eu
		 **/
		void dansGrille(pair<int,int> size);
		
		bool changeOccur(pair<int,int> size);
		
	private:
		int m_numero;
		std::pair<int,int> m_coord;
		dir m_direction;
		bool m_brindille;
		int m_sablier;


}; 
#endif
