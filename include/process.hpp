#include<bits/stdc++.h>
#include "Grille.hpp"
#include "Termite.hpp"
#include "environement.hpp"
#include "console.hpp"

using namespace std;

/** effectue un tour pour tous les termites
 * @param[in/out] la grille 
 * @param[in/out] un vecteur de termite 
 **/
void passTurn(grille &g, std::vector<termite> &t);

/** permet la gestion des brindilles i.e ramasser et déposer 
 * @param[in/out] la grille 
 * @param[in/out] un termite 
 **/
void brindilleManage(grille &g, termite &t);


/** Generation des termite 
 * @param[in/out] la grille 
 * @return un vecteur de termite 
 **/
 vector<termite> generationTermite(grille &g);

/** Generation des brindille 
 * @param[in/out] la grille  
 **/
void generationBrindille(grille &g);

/** gestion principale de la partie
 **/
void mainManagment();