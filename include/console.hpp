#include <bits/stdc++.h>
#include "environement.hpp"
#include "Grille.hpp"
#include "Termite.hpp"
using namespace std;

/** affiche l'etat de la grille pour un "tour" de deplacement 
 * @param[in] un tableau de termite 
 * @param[in] un tableau de brindille
 **/
void display(vector<termite> t,grille g);

/**Met en forme la manière dont sera affiché les termites
 * @param[in] un termite 
 * @return un flux contenu dans le buffer *
 **/
ostringstream displayTermite(termite t);

/** modifie la position du curseur dans la console
 * deux entier x et y corrspondant aux lignes et colonnes selectionnes
 **/
void cursorPos(int row, int column);

