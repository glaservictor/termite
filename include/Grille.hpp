#include <bits/stdc++.h> 

#ifndef GRILLE_HPP
#define GRILLE_HPP

#define l 20
#define L 20
#define case std::pair<int,int>

//////////////////////////////////////////////////////////
// 		           fonctions du type   				    //
//////////////////////////////////////////////////////////


class grille {
	public:
		/////////////////   METHODE D'ACCES  /////////////////

		/** Constructeur de la classe grille initialise un tableau vide
		 */
		grille();

		/** Test si la grille est vide ou non 
		 * @param[in] la grille 
		 * @return vrai si la grille ne contient ni brindit ni termite a la case donnée
		 **/
		bool estVide();

		/** Test si la position est bien dans la grille
		 * @return vrai si la case est bien dans la grille 
		 **/
		case dansGrille(case c);


		/** Test si une case c contient une brindille ou non
		 * @param[in] un entier 
		 * @return Vrai si la case c contient bien une brindille 
		 **/
		bool contientBrindille(case c);
		
		/** renvoie le numero de termite a une case
		 * @param[in] la case recherchée 
		 * @return le numero du termite ou -1 sinon
		 **/
		int numeroTermite(case c);
		
		////////////////  PROCEDURE DE MODIFICATION ///////////////
		
		/** pose une brindille sur la case indiquée 
		 * @param[in] une case 
		 **/
		void poseBrindille(case c);

		/** enleve une brindille sur la case idniquée 
		 * @param[in] une case 
		 * 
		 **/
		void eneleveBrindille(case c);

		/** pose une termite sur une case c une termite n
		 * @param[in/out] un vecteur contenant toutes les brindilles
		 * @param[in] une case c
		 * @param[in] une termite n
		 **/
		void poseTermite(case c, int n);

		/** eneleve une termite n d'une case c
		 * @param[in] une case 
		 **/
		void eneleveTermite(case c);
		
		/** test si une case specifique est vide 
		 * @param[in/out] un vecteur de brindille 
		 * @param[in] un pair d'entier correspondant aux coordonées 
		 * @return true si la case est bien vide
		 **/
		bool estVide(std::pair<int,int>);

		/** stock la dimension de la grille en question 
		 **/
		std::pair<int,int> getDim();

		/** accesseur sur la grille
		 * @return un vecteur de vecteur d'entier
		 **/
		std::vector<std::vector<int>> accesGrille(); 

	private:
		std::vector<std::vector<int>> Grille;
		std::pair<int,int> size = {l,L};




};

#endif 
