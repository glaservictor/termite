#include <bits/stdc++.h>
#include "Termite.hpp"
#include "Grille.hpp"

//////////////////////////////////////////////////////////////
//       Utilisation de la grille et des termites           // 
/////////////////////////////////////////////////////////////





//////////    fonction d'action sur ces 2 classes  ///////////




/** observe si la case devant le termite est libre
 * @param[in] une grille 
 * @param[in] une termite 
 * @return vrai si la voie est libre 
 **/
bool laVoieEstLibre(grille g, termite t);

/** test si une brindille est posée en face d'une termite 
 * @param[in] une grille 
 * @param[in] une termite 
 * @param[in] un veteur de brindille 
 * @return vrai si une brindille est bein en face de la termite 
 **/
bool brindilleEnFace(grille g, termite &t);

/** verifie que la termite ne s'enferme pas en posant la brindille sur une case
 * @param[in] une grille 
 * @param[in] une termite 
 * @return vrai si la termite ne s'enferme pas 
 **/
bool pasEnferme(grille g, termite &t);


////////////    procédures modifiant l'environement ///////////


/** fait avancer la termite d'une case devant elle 
 * @param[in/out] une grille g
 * @param[in/out] une termite t
 **/
void avanceTermite(grille& g, termite& t);

/** la termite pose la brindille sur la case devant elle 
 * @param[in/out] une grille g
 * @param[in/out] une termite t
 **/
void dechargeBrindille(grille &g, termite &t);

/** la termite prends la brindille de la case situé devant lui 
 * @param[in/out] une grille g
 * @param[in/out] une termite t
 **/
void chargeBrindille(grille &g, termite &t);

/** le termite se deplace de manière aléatoire sur un deplacement
 * @param[in/out] une grille g
 * @param[in/out] une termite t
 **/
void marcheAleatoire(grille &g, termite &t);

/** permet la condition d'accès de la brindille 
 * @param[in] un vecteur de brindille
 * @param[in] un termite 
 * @param[in] la grille 
 * @param[in] un booléens true pour la charge false pour la decharge 
 * @return vrai ou faux 
 **/
bool accessBridnille(grille &g, termite &t);

/** determine la position libre pour une termite 
 * @param[in] un termite 
 * @param[in] la grille
 * @return true si il existe une position libre 
 **/
bool positionLibre(grille &g, termite &t);


/** denombre la quntité de brins sur la grille **dediée au tests**
 * @param[in] la grille g
 * @param[in] un vecteur de termite 
 * @return le nombre de brins
 **/
int nbBrin(vector<termite> t, grille g);

/** checkout the presence of a brindille at the bord of the scren 
 * @param[in] la grille g
 * @param[in] un vecteur de termite 
 * @return true si brindille il y a 
 **/
bool brindilleBord(grille g, termite t);