#include <bits/stdc++.h>
#include "include/Termite.hpp"
#include "include/Grille.hpp"
#include "include/environement.hpp"

#define TIMER 6

using namespace std;

bool laVoieEstLibre(grille g, termite t)
{
	if(t.changeOccur(g.getDim()))
	{
		t.dansGrille(g.getDim());
		return g.estVide(t.accessCoord());
	}
		return (g.estVide(t.devantTermite()));
}

bool brindilleBord(grille g, termite t)
{
	t.dansGrille(g.getDim());
	return (g.contientBrindille(t.accessCoord()) && t.sablier()>TIMER);
}

bool brindilleEnFace(grille g, termite &t)
{

	for(int i = 0; i < 8; i++)
	{
	t.tourneLibre(i);
	pair<int,int> emp = t.devantTermite();
	if (g.contientBrindille(emp))
		return true;
	}
	return false;
}

bool pasEnferme(grille g, termite &t){
	int k = t.directionTermite(),counter = 0;
	for(int i = 0; i<8; i++)
	{
		if(i==k) continue;
		t.tourneLibre(i);
		if(t.changeOccur(g.getDim()))
			if(g.estVide(t.devantTermite())) counter++;
	}
	return (counter>=2);
}

bool accessBridnille(grille &g, termite &t){
	for(int i = 0; i< 8; i++){
		t.tourneLibre(i);
		if(t.changeOccur(g.getDim()))
			return brindilleBord(g,t);
		else if (g.contientBrindille(t.devantTermite()) && t.sablier()>TIMER)
			return true;
	}
	return false;
}

bool positionLibre(grille &g, termite &t)
{
	int k = 0;
	while(!laVoieEstLibre(g,t))
	{
		t.tourneAGauche();
		if(k>8) return false;
	}
	return true;
}

void avanceTermite(grille &g, termite &t)

{	

	if(laVoieEstLibre(g,t))
	{
		int c = t.numero();
		g.eneleveTermite(t.accessCoord());
		if(t.changeOccur(g.getDim())) t.dansGrille(g.getDim());
		else t.avance();
		g.poseTermite(t.accessCoord(),c);
	}
	else if(accessBridnille(g,t) && !t.porteBrindille())
		chargeBrindille(g,t);
	else if(accessBridnille(g,t) && t.porteBrindille())
		dechargeBrindille(g,t);
	else
		marcheAleatoire(g,t);
		
}

void dechargeBrindille(grille &g, termite &t)

{
	positionLibre(g,t);	
	termite te = t;
	if(t.changeOccur(g.getDim())) {te.dansGrille(g.getDim()); g.poseBrindille(te.accessCoord());}
	else g.poseBrindille(t.devantTermite());
	t.poseBrindille();
}

void chargeBrindille(grille &g, termite &t)

{
	g.eneleveBrindille(t.devantTermite());
	t.prendBrindille();
	t.avance();
}

void marcheAleatoire(grille &g, termite &t)

{ 
	if(positionLibre(g,t))
	{
		while(!(laVoieEstLibre(g,t)))
		{
			t.tourneAleat();
		}
	avanceTermite(g,t);
	}
	else
		t.tourneAGauche();

	
}

int nbBrin(vector<termite> t, grille g){
    int k = 0;
    for(termite &te : t)
        if(te.porteBrindille())
            k+=1;
    for(vector<int> &gr: g.accesGrille())
        for(int &gri: gr)
            if(gri==-1)
                k+=1;
    return k;
}

