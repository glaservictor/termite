#include<bits/stdc++.h>
#include <stdlib.h> 
#include <ctime> 
#include <cstdlib>
#include <time.h>
#include "include/environement.hpp"
#include "include/console.hpp"
#define brindille 20
#define term 10

using namespace std;

vector<termite> generationTermite(grille& g)
{
	srand((unsigned)time(0));
	int x = 0, y = 0;
	vector<termite> ter;
	vector<vector<bool>> vu(g.getDim().first);
	for(vector<bool> &b : vu) b = vector<bool>(g.getDim().second);
	for(int i = 1; i<= term; i++)
		{
			do{
				x = rand()% g.getDim().first,y= rand() % g.getDim().second;
			}while(vu[x][y]);
			termite t(x,y,i) ;g.poseTermite({x,y},i); ter.push_back(t);vu[x][y] = true;
		}
	return ter;
}

void generationBrindille(grille &g)
{
	srand((unsigned)time(0));
	int x = 0,y=0;
	vector<vector<bool>> vu(g.getDim().first);
	for(vector<bool> &v : vu)
		v = vector<bool>(g.getDim().first,false);
	for(int i = 0; i< brindille; i++)
	{
			do
			{
				 x = rand()% g.getDim().first,y= rand() % g.getDim().second;
			}while(vu[x][y]);
			g.poseBrindille({x,y}) ; vu[x][y] = true;
	}
}


void passTurn(grille& g, vector<termite>& t)
{
	srand(time(0));
	int alea = 0;
	for(termite &te : t)
	{
		alea = rand()%2;
		if(alea == 1) marcheAleatoire(g,te);
		else avanceTermite(g,te);
	}
}


void mainManagment()
{
	
int c = 0;
int p = 100;
grille g;
vector<termite> ter = generationTermite(g);
generationBrindille(g);
puts ("Enter text. Include ('.') to exit or any key to continue\n ** tips : press a for fast execution ** \n >'*' to make it quicky \n >'/' for slower execution");
c = getchar();

	do
	{
		//if(nbBrin(ter,g) != brindille)
		//{break; cout<<"disparition impromptue";}
		display(ter,g);
		c=getchar();
		if(c=='*') p*=2;
		if(c=='/') p/=2;
		if(c=='a' or c =='/' or c == '*') 
			for(int i = 0; i<p;i++)
				{ 
					passTurn(g,ter);
					display(ter,g);
				}
		putchar(c);
		passTurn(g,ter);
		
	}
	while(c != '.');
}
