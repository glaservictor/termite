#include <bits/stdc++.h>
#include "include/environement.hpp"
#include "include/Grille.hpp"
#include "include/Termite.hpp"
#include "include/console.hpp"


using namespace std;

ostringstream displayTermite(termite t){
	ostringstream ss;
	if (t.porteBrindille()) ss<<"\x1b[31m";
	else if(!t.porteBrindille()) ss<<"\x1b[34m";
	if(t.directionTermite()==0) ss<<"\\";  
	if(t.directionTermite()==1) ss<<'-';
	if(t.directionTermite()==2) ss<<'|';
	if(t.directionTermite()==3) ss<<"/";
	if(t.directionTermite()==4) ss<<"\\";
	if(t.directionTermite()==5) ss<<'|';
	if(t.directionTermite()==6) ss<<"/";
	if(t.directionTermite()==7) ss<<'-';
	return ss;

}

void cursorPos(int row, int column)
{
	printf("\x1b[%d;%df",column+1,row+1);
}

void display(vector<termite> t, grille g)
	
{
	printf("\x1b[2J");
	for(termite &te: t)
	{
		ostringstream ss; 
		ss = displayTermite(te);
		cursorPos(te.accessCoord().second, te.accessCoord().first);
		cout<<ss.str();
		printf("\x1b[00m");
	}
	vector<vector<int>>  G = g.accesGrille();
	for(int i = 0; i < G.size(); i++)
		for(int j = 0; j< G[0].size();j++)
	{
		if(G[i][j]==-1)
		{
		cursorPos(j,i);
		cout<<'*';
		}
	}
	printf("\x1b[u");
}