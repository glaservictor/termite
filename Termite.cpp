#include "./include/Termite.hpp"
#include <bits/stdc++.h>
#include <cstdlib>

using namespace std;

termite::termite(int n,int m,int i)

{
	m_coord = {n,m};
	m_brindille = 0;
	m_numero = i;
	m_sablier = 0;
	m_direction = dir(rand()% 8);
}

dir termite::directionTermite()
{
	return m_direction;
}

int termite::tempsBrin()
{return m_sablier;}

pair<int,int> termite::devantTermite()
{
	pair<int,int> copcord;
	if(m_direction ==0) copcord = {m_coord.first-1,m_coord.second-1};
	if(m_direction ==1) copcord = {m_coord.first, m_coord.second+1};
	if(m_direction ==2) copcord = {m_coord.first-1, m_coord.second};
	if(m_direction ==3) copcord = {m_coord.first+1, m_coord.second-1};
	if(m_direction ==4) copcord = {m_coord.first+1, m_coord.second+1};
	if(m_direction ==5) copcord = {m_coord.first+1,m_coord.second};
	if(m_direction ==6) copcord ={m_coord.first-1,m_coord.second+1};
       	if(m_direction ==7) copcord ={m_coord.first, m_coord.second-1};

	return copcord;
}

bool termite::porteBrindille()
{
	return m_brindille;
}

int termite::sablier()
{
	return m_sablier;
}

void termite::avance()
{

	m_coord = devantTermite();
	m_sablier +=1;

}

void termite::tourneADroite()
{
	if(m_direction == 7) m_direction = (dir)0;
	else m_direction = dir(m_direction + 1);

}

void termite::tourneLibre(int c)

	{
		if(c>7) c%=7;
		m_direction = (dir)c;
	}

void termite::tourneAGauche()
{
	if(m_direction == 0) m_direction = (dir)7;
	else m_direction = dir(m_direction - 1);
}

void termite::tourneAleat()
{
	m_direction = dir(rand() % 7);	
}

pair<int,int> termite::accessCoord()
{
	return m_coord;

}
void termite::prendBrindille()
{
	m_brindille = true;
	m_sablier = 0;
}

int termite::numero()
{
	return m_numero;
}

void termite::poseBrindille()
{
	m_brindille = false;
	m_sablier = 0;
}
void termite::dansGrille(pair<int,int> size)

{
	if(devantTermite().first>=size.first) m_coord.first = 0;
	if(devantTermite().second>=size.second) m_coord.second = 0;
	if(devantTermite().second<0) m_coord.second = size.second -1;
	if(devantTermite().first<0) m_coord.first = size.first -1; 
}

bool termite::changeOccur(pair<int,int> size)
{
	pair<int,int> cop = m_coord;
	dansGrille(size);
	return(cop != m_coord);
}