CXX = g++
CXXFLAGS =  -std=c++17 -g
DIR = ./object ./
OBJDIR = object
LIBG = -lsfml-system -lsfml-window -lsfml-graphics
SRC = $(foreach dir, $(DIR), $(wildcard $(dir)/*.cpp))
OBJ = $(addprefix $(OBJDIR)/, $(patsubst %.cpp, %.o, $(filter-out mainG.cpp mainT.cpp, $(wildcard *.cpp))))
OBJG = $(addprefix $(OBJDIR)/,$(patsubst %.cpp, %.o,mainG.cpp)) 
OBJT = $(addprefix $(OBJDIR)/, $(patsubst %.cpp, %.o,mainT.cpp))
LIB = $(wildcard include/*.hpp)
EXECG = Gtermite
EXECT = Ttermite

all :$(OBJDIR) $(EXECG) $(EXECT)

$(OBJDIR):
	mkdir $(OBJDIR)

$(OBJDIR)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(EXECG): $(OBJ) $(OBJG)
	$(CXX) -o $@ $^ $(LIBG) $(CXXFLAGS)

$(EXECT): $(OBJ) $(OBJT)
	$(CXX) -o $@ $^ $(LIBG) $(CXXFLAGS)

.PHONY: mrproper clean all 

clean : 
	$(foreach dir, $(DIR) , rm -rf $(wildcard $(dir)/*.o))

mrproper:
	rm -rf $(EXECG) $(EXECT) 
