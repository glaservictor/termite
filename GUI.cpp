#include <SFML/Graphics.hpp>
#include <bits/stdc++.h>
#include "include/Termite.hpp"
#include "include/Grille.hpp"
#include "include/process.hpp"
#include "include/GUI.hpp"

using namespace sf;
using namespace std;

void dimTerrain(grille &g, int &lc, int &Lc)
{
    int h = g.getDim().first, w = g.getDim().second;
    lc = 600/h; Lc = 600/w;
}

Rect<float> setOrigin(Sprite &s)
{
  
    Rect<float> size = s.getGlobalBounds();
    s.setOrigin(Vector2f(size.width/2., size.height/2.));
    return size;
}




int termRot(Sprite &s, dir d = ouest)
{
    if(d == ouest) s.setRotation(-90.f);
    if(d==sud) s.setRotation(180.f);
    if(d == est) s.setRotation(90.f);
    if(d == nord) s.setRotation(0.f);
    if(d == nordOuest) s.setRotation(-45.f);
    if(d==nordEst) s.setRotation(45.f);
    if(d== sudEst) s.setRotation(135.f);
    if(d == sudOuest) s.setRotation(-135.f);
    return 0;
}

vector<sf::Sprite> initialiseSpriteBrindille(grille g)
{
    vector<sf::Sprite> S;
    for(int i = 0; i < g.getDim().first; i++)
        for(int j = 0; j < g.getDim().second; j ++)
            if(g.contientBrindille({i,j}))
            {
                Sprite s;
                s.setScale(Vector2f( 0.2*(float)1/g.getDim().first,0.2*(float) 1/g.getDim().second ));
                S.push_back(s);
            }
    return S; 
}



vector<sf::Sprite> initialiseSpriteTermite(vector<termite> t, grille g)
{
    vector<sf::Sprite> S;
    for(int i = 0; i < t.size(); i++)
    {
        Sprite s;
        s.setScale(Vector2f( (float)1/g.getDim().first, (float) 1/g.getDim().second ));
        S.push_back(s);
    }
    return S;
}


vector<pair<int,int>> findBrindille(grille g)
{
    vector<pair<int,int>> P;
    for(int i = 0; i < g.getDim().first; i++)
        for(int j = 0; j < g.getDim().second; j ++)
                if(g.contientBrindille({i,j}))
                    P.push_back({j,i});
    return P;
}

void updateBrindille(vector<pair<int,int>> C, vector<Sprite> &sB,int lc, int Lc)
{
    for(int i = 0; i<sB.size(); i++)
    {
        if(i<C.size())
            sB[i].setPosition(Vector2f(C[i].first*Lc, C[i].second*lc));
        else 
            sB[i].setPosition(Vector2f(-100,-100));
    }
}



void updatePosition(grille g,vector<termite> t, vector<sf::Sprite> &sT,vector<sf::Sprite> &sB,int lc, int Lc)
{
    for(int i = 0; i < t.size(); i++){
        sT[i].setPosition(Vector2f(t[i].accessCoord().second*lc,t[i].accessCoord().first*Lc));
        termRot(sT[i], t[i].directionTermite());
    }
    vector<pair<int,int>> p = findBrindille(g);
    updateBrindille(p, sB,lc,Lc);
}


int windowManage()
{
    bool pause = false;
    int turn = 10;
    grille g;
    vector<termite> t = generationTermite(g);
    generationBrindille(g);
    int lc = 0, Lc = 0;
    dimTerrain(g,Lc,lc);
    Texture textureT;
    Texture textureB;
    Texture textureTa;

    if(!textureT.loadFromFile("./.texture/termite.png") or 
    !textureB.loadFromFile("./.texture/lead.png") or
     !textureTa.loadFromFile("./.texture/termiteBuisy.png"))
    {std::cerr<<" DOWNLOAD TEXTURE PACKAGE"; return -2;}

    sf::Font font;
    if (!font.loadFromFile("./.police/antonio.ttf")){cerr<<"ERREUR CHARGEMENT POLICE";return -3;}

    vector<sf::Sprite> St = initialiseSpriteTermite(t,g);
    vector<sf::Sprite> Sb = initialiseSpriteBrindille(g);

    for(sf::Sprite &s : St)
        s.setTexture(textureT);
    for(sf::Sprite &s : Sb)
        s.setTexture(textureB);

    updatePosition(g,t,St,Sb,Lc,lc); 

    RenderWindow window(VideoMode(600,600), "Termite Simulation");
    while (window.isOpen())
    {
        for(int i = 0; i < St.size(); i++)
        {
            if(t[i].porteBrindille())
                St[i].setTexture(textureTa);
            else
                St[i].setTexture(textureT);
        }        

  
        Event event;
        if(!pause){
        window.clear(sf::Color(255,255,255));
        Text text;
        text.setFont(font);
        text.setString(">Press any key to continue \n>'Right' for quick moove\n>'Up' To go even faster \n>'Down' to slow down a bit ");
        text.setColor(Color::Black);
        window.draw(text);
        window.display();
        while (window.pollEvent(event))
        {
            if (event.type == Event::KeyPressed)
                {
                    pause = true;
                    goto manage;
                }
            }
        }
        manage:
        if(pause)
        while (window.pollEvent(event) && pause)
        {
            if (event.type == Event::Closed or (event.type == Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
                window.close();
            if(event.type == Event::KeyPressed && event.key.code == Keyboard::Up)
                {if(turn == 0) turn++; else turn*=2;goto manage;}
            if(event.type == Event::KeyPressed && event.key.code == Keyboard::Down)
                {turn/=10;goto manage;}
            if(event.type == Event::KeyPressed && event.key.code == (Keyboard::Right))
            {
                for(int i = 0; i < turn; i ++)
                    passTurn(g,t);
                goto manage;
            }
            if(event.type == Event::KeyPressed)
                passTurn(g,t); 
            updatePosition(g,t,St,Sb,Lc,lc);
             window.clear(sf::Color(255,255,255));
            for(Sprite &s : Sb)
                window.draw(s);
            for(Sprite &t :St)
                window.draw(t);
            window.display();   
        }  
    }
    return 0;
}
